/* custom scripts - martin [naicode.com]*/
(() => {
	//readystate check
	window.readyHandlers = [];
	window.handleState = () => { if (['interactive', 'complete'].indexOf(document.readyState) > -1) while(window.readyHandlers.length > 0) (window.readyHandlers.shift())(); };
	window.ready = (handler) => { window.readyHandlers.push(handler); handleState(); };
	document.onreadystatechange = window.handleState;

	//custom
	ready(() => {
		//thanks to https://technext.github.io/places/onepage.html
		let OnePageNav = function(){
			$("a[href^='#']").on('click', function(e){
				e.preventDefault();
				let hash = this.hash;
				if (hash == "#x-offcanvas-menu") return;
				let hash_top = $(hash).offset().top;
				let this_top = $(this).offset().top;
				if (this_top > hash_top){
					$('html, body').animate({scrollTop: hash_top - 100 }, 700, 'easeInOutExpo');
					$('html, body').animate({scrollTop: hash_top }, 700, 'easeInOutExpo');
				}
				else $('html, body').animate({scrollTop: hash_top }, 700, 'easeInOutExpo');
			});
			console.log("OnePageNav init");
		};
		OnePageNav();
	});
})();
